import functions  from '../src/functions';

test('test sum func', () => {
    expect(functions.sum(2, 2)).toBe(4);
})
